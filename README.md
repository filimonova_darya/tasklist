# tasklistapp

## Инструкция по разворачиванию:

### Сервер
- перейти в ./server и вызвать
`npm install`
- вызвать
`node server.mjs`
или использовать скрипт в package.json 
`start:server`


### Клиент
- перейти в ./client и вызвать
`npm install`
- вызвать
`vue-cli-service serve`
или использовать скрипт в package.json 
`start:client`
